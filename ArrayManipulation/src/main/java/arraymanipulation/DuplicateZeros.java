/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package arraymanipulation;

/**
 *
 * @author Nobpharat
 */
import java.util.Scanner;

public class DuplicateZeros {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the number of elements in the array: ");
        int n = scanner.nextInt();

        int[] arr = new int[n];
        System.out.print("Enter the elements of the array:");
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        scanner.close();

        System.out.print("Input: [");
        outArray(arr);
        duplicateZeros(arr);
        System.out.print("\nOutput: [");
        outArray(arr);
        System.out.println();
    }

    private static void duplicateZeros(int[] arr) {
        int count = 0;
        int length = arr.length - 1;

        for (int i = 0; i <= length - count; i++) {
            if (arr[i] == 0) {
                count++;
            }
        }
        for (int i = length - count; i >= 0; i--) {
            if (arr[i] == 0) {
                arr[i + count] = 0;
                count--;
                arr[i + count] = 0;
            } else {
                arr[i + count] = arr[i];
            }
        }
    }

    private static void outArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (i < arr.length - 1) {
                System.out.print(arr[i] + ", ");
            } else {
                System.out.print(arr[i]);
            }
        }
        System.out.print("]");
    }
}

