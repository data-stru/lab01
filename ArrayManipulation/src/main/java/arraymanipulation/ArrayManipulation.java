/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package arraymanipulation;

/**
 *
 * @author Nobpharat
 */
public class ArrayManipulation {

    public static void main(String[] args) {
        int[] numbers = {5, 8, 3, 2, 7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        Double[] values = new Double[4];
        Double sum = 0.0;
        Double max = 0.0;
        
        System.out.print("numbers : ");
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i]+" ");
        }
        System.out.print("\nnames : ");
        for (int i = 0; i < names.length; i++) {
            System.out.print(names[i]+ " ");
        }

        values[0] = 1.01;
        values[1] = 1.02;
        values[2] = 1.03;
        values[3] = 1.04;

        for (int i = 0; i < values.length; i++) {
            sum = sum + values[i];
        }

        System.out.printf("Sum of values: %.2f%n", sum);

        for (int i = 0; i < values.length; i++) {
            if (max < values[i]) {
                max = values[i];
            }
        }

        System.out.println("Max of values : " +max);
        
        System.out.print("reversedNames :  ");
        String[] reversedNames = new String[names.length];
        for (int i = 0; i < reversedNames.length; i++) {
            reversedNames[i] = names[names.length - (1 + i)];
            System.out.print(reversedNames[i]+ " " );
        }
        
        
        System.out.print("\nsorted \"numbers\" : ");
        for (int i = 0; i < numbers.length - 1; i++) {
            for (int j = 0; j < numbers.length - i - 1; j++) {
                if (numbers[j] > numbers[j + 1]) {
                    int revNumbers = numbers[j];
                    numbers[j] = numbers[j + 1];
                    numbers[j + 1] = revNumbers;
                }
            }
        }
        for (int i = 0; i < numbers.length - 1; i++) {
            System.out.print(numbers[i] + " ");
        }
        
      

    }
}
