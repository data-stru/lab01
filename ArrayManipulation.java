public class ArrayManipulation {
    public static void main(String[] args) throws Exception {
        int[] numbers = {5,8,3,2,7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        Double[] values = new Double[4];
        Double sum = 0.0;
        Double max = 0.0;

        for(int i=0; i < numbers.length; i++){
            System.out.println(numbers[i]);
        }

        for(int i=0; i < names.length; i++){
            System.out.println(names[i]);
        }

        values[0] =1.01;
        values[1] =1.02;
        values[2] =1.03;
        values[3] =1.04;

        
        for(int i=0; i < values.length; i++){
            sum = sum + values[i];
        }

        System.out.println(sum);

        for(int i=0; i < values.length; i++){
            if (max < values[i]){
                max = values[i];
            }
        }

        System.out.println(max);


        
        String[] reversedNames = new String[names.length];
        for(int i = 0; i < reversedNames.length;i++){
            reversedNames[i] = names[names.length-(1+i)];
            System.out.println(reversedNames[i]);
        }

        
    }
}
